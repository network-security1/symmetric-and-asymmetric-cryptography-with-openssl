if [ $# -ne 4 ];
then
  echo "usage: $0 <zip_name> <signature_file> <priv_key> <pub_key>"
  exit -1
fi


function decrypt ()
{
  for i in `ls`
  do
    openssl rsautl -decrypt -in $i -out "${i:0:-4}" -inkey ../$1
  done

}

function checkSignature() {
  if openssl rsautl -verify -in ../$1 -inkey ../$2 -pubin; then
      return 0
  else
      return -1
  fi
}

function unzipItems()
{
  file=$1
  mkdir decrypt
  cd decrypt
  unzip ../$1 -d ./
}

unzipItems $1

if checkSignature $2 $4; then
  decrypt $3
else
  echo "non valid signature"
  exit -1
fi

cd ..
