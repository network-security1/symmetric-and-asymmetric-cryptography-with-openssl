if [ "$#" -ne 2 ];
then
	echo "usage: $0 <key_size> <out_private_key_file> <out_public_key_file>"
	exit -1
fi
PRIV=$2
PUB=$3

openssl genrsa -out $PRIV $1
openssl rsa -in $PRIV -pubout -out $PUB
echo "protect you priv key with aes256 [y/n]"
read encrypt
if [ $encrypt == "y" ]; then
	openssl rsa -in $PRIV -aes256 -out $PRIV
fi
