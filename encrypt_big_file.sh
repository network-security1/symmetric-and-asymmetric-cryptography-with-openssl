if [ $# -ne 4 ];
then
  echo "usage: $0 <key_size> <file_name> <pub_key> <priv_key>"
  exit -1
fi

let size=$1/8-40

function encrypt ()
{
  for i in `ls`
  do
    openssl rsautl -encrypt -in $i -out "$i.enc" -inkey ../$1  -pubin
  done
  zip encrypted *.enc > /dev/null
  mv encrypted.zip ../
}

function sign()
{
  openssl dgst -sha256 ../$1 > signature.sign
  openssl rsautl -sign -in signature.sign -out "../signature.enc" -inkey ../$2
}



function splitItems()
{
  file=$1
  mkdir splited
  cd splited
  split -b$size ../$file
}

splitItems $2
encrypt $3
sign $2 $4
cd ..
rm -r splited
