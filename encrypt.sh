if [ "$#" -ne 5 ]; then
	echo "usage: $0 <input_file> <encripted_file> <signature_out> <pub_key_file> <priv_key>"
	exit 1
fi
# Encrypt the file
openssl rsautl -encrypt -in $1 -out $2 -inkey $4 -pubin

if 
	# Sign the file 
	openssl rsautl -sign -in $1 -out $3 -inkey $5

