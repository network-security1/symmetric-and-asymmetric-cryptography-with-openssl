if [ "$#" -ne 3 ]; then
	echo "usage: $0 <input_file> <output_file> <pub_key>"
	exit
fi

if [ openssl rsautl -verify -in $1 -out $2 -inkey $3 -pubin ]; then
	echo "ok"
else
	echo "bad"
fi
